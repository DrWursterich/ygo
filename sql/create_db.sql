DROP TABLE IF EXISTS Archetype;
DROP TABLE IF EXISTS BannedState;
DROP TABLE IF EXISTS Card;
DROP TABLE IF EXISTS CardAttribute;
DROP TABLE IF EXISTS CardRace;
DROP TABLE IF EXISTS CardType;
DROP TABLE IF EXISTS DetailedType;
DROP TABLE IF EXISTS Meta;
DROP TABLE IF EXISTS Rarity;
DROP TABLE IF EXISTS Product;
DROP TABLE IF EXISTS ProductEntry;

CREATE TABLE Rarity(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	name TEXT NOT NULL,
	UNIQUE(name)
);

CREATE TABLE Product(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	name TEXT NOT NULL,
	UNIQUE(name)
);

CREATE TABLE ProductEntry(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	card REFERENCES Card NOT NULL,
	product REFERENCES Product NOT NULL,
	code TEXT NOT NULL,
	rarity REFERENCES Rarity NOT NULL,
	price FLOAT
);

CREATE TABLE Meta(
	id INTEGER PRIMARY KEY,
	time_of_update DATE NOT NULL,
	api_version REAL NOT NULL
);

CREATE TABLE Card(
	id INTEGER PRIMARY KEY,
	name TEXT NOT NULL,
	type REFERENCES DetailedType(id) NOT NULL,
	description TEXT NOT NULL,
	attribute REFERENCES CardAttribute,
	race REFERENCES CardRace(id) NOT NULL,
	atk INTEGER,
	def INTEGER,
	level INTEGER,
	archetype REFERENCES Archetype(id),
	/* staple BOOLEAN NOT NULL, */
	/* treated_as REFERENCES Card(id), */
	staple TEXT,
	treated_as TEXT,
	release_date DATE NOT NULL,
	tcg_banned_state REFERENCES BannedState(id) NOT NULL
);

CREATE TABLE CardType(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	name TEXT NOT NULL,
	UNIQUE(name)
);

CREATE TABLE DetailedType(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	name TEXT NOT NULL,
	general_type REFERENCES CardType(id) NOT NULL,
	UNIQUE(name)
);

CREATE TABLE CardAttribute(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	name TEXT NOT NULL,
	UNIQUE(name)
);

CREATE TABLE CardRace(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	name TEXT NOT NULL,
	UNIQUE(name)
);

CREATE TABLE Archetype(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	name TEXT NOT NULL,
	UNIQUE(name)
);

CREATE TABLE BannedState(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	name TEXT NOT NULL,
	max_amount INT NOT NULL,
	UNIQUE(name)
);

INSERT INTO BannedState(name, max_amount)
	VALUES
		("Forbidden", 0),
		("Limited", 1),
		("Semi-Limited", 2),
		("Unlimited", 3);

INSERT INTO CardType(name)
	VALUES
		("Monster"),
		("Trap"),
		("Spell"),
		("Extra"),
		("Invalid");
