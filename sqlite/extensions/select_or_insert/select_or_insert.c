#include <assert.h>
#include <sqlite3ext.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

/*
 * provides an SELECT_OR_INSERT(string table, string name) function for an
 * sqlite3 database.  this function returns the id of an row with the given name
 * from the given table.  if none is present it is inserted first and it's id is
 * returned afterwards.
 *
 * example:
 * CREATE TABLE TableA(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT);
 * CREATE TABLE TableB(id INTEGER PRIMARY KEY AUTOINCREMENT, a REFERENCES TableA(id));
 * INSERT INTO TableB(a) VALUES (SELECT_OR_INSERT("TableA", "test"));
 * SELECT * FROM TableB;
 * -- ┌────┬───┐
 * -- │ id │ a │
 * -- ├────┼───┤
 * -- │ 1  │ 1 │
 * -- └────┴───┘
 * SELECT * FROM TableA;
 * -- ┌────┬──────┐
 * -- │ id │ name │
 * -- ├────┼──────┤
 * -- │ 1  │ test │
 * -- └────┴──────┘
 *
 * also provides an SELECT_OR_INSERT_CARD_TYPE(string detailed) function,
 * which is basically a SELECT_OR_INSERT function for the table "DetailedType".
 *
 * to load the compiled .so (on unix systems) see:
 * https://sqlite.org/loadext.html#loading_an_extension
 *
 * for error codes returned from the sqlite interface see:
 * https://sqlite.org/rescode.html#result_code_meanings
 */

SQLITE_EXTENSION_INIT1

static void select_or_insert(sqlite3_context *ctx, int argc, sqlite3_value **argv) {
	const char *table, *name;
	sqlite3 *db;
	sqlite3_stmt *stmt;
	char *sql;
	int rc;

	assert(argc == 2);

	table = (const char *) sqlite3_value_text(argv[0]);
	if (!table) {
		sqlite3_result_error(ctx, "missing table", -1);
		return;
	}
	name = (const char *) sqlite3_value_text(argv[1]);
	if (!name) {
		sqlite3_result_null(ctx);
		return;
	}

	db = sqlite3_context_db_handle(ctx);

	sql = sqlite3_mprintf("SELECT id FROM %s WHERE name=?", table);
	rc = sqlite3_prepare_v2(db, sql, -1, &stmt, NULL);
	rc |= sqlite3_bind_text(stmt, 1, name, -1, SQLITE_STATIC);
	if (rc != SQLITE_OK) {
		char str[256];
		sprintf(str, "preparing select failed: %d - %s", rc, sqlite3_errmsg(db));
		sqlite3_result_error(ctx, str, -1);
	} else if ((rc = sqlite3_step(stmt)) == SQLITE_ROW) {
		sqlite3_result_int64(ctx, sqlite3_column_int64(stmt, 0));
	} else if (rc == SQLITE_DONE) {
		sqlite3_free(sql);
		sqlite3_finalize(stmt);
		sql = sqlite3_mprintf("INSERT INTO %s(name) VALUES (?)", table);
		rc = sqlite3_prepare_v2(db, sql, -1, &stmt, NULL);
		rc |= sqlite3_bind_text(stmt, 1, name, -1, SQLITE_STATIC);
		if (rc != SQLITE_OK) {
			char str[256];
			sprintf(str, "preparing insert failed: %d - %s", rc, sqlite3_errmsg(db));
			sqlite3_result_error(ctx, str, -1);
		} else if ((rc = sqlite3_step(stmt)) == SQLITE_DONE) {
			sqlite3_result_int64(ctx, sqlite3_last_insert_rowid(db));
		} else {
			char str[256];
			sprintf(str, "inserting failed: %d - %s", rc, sqlite3_errmsg(db));
			sqlite3_result_error(ctx, str, -1);
		}
	} else {
		char str[256];
		sprintf(str, "selecting failed: %d - %s", rc, sqlite3_errmsg(db));
		sqlite3_result_error(ctx, str, -1);
	}
	sqlite3_free(sql);
	sqlite3_finalize(stmt);
	return;
}

static char *generalize_type(const char *detailed) {
	if (strcmp(detailed, "Spell Card") == 0) {
		return "Spell";
	} else if (strcmp(detailed, "Trap Card") == 0) {
		return "Trap";
	} else if (strstr(detailed, "Fusion")
			|| strstr(detailed, "Synchro")
			|| strstr(detailed, "XYZ")
			|| strstr(detailed, "Link")) {
		return "Extra";
	} else if (strstr(detailed, "Monster")) {
		return "Monster";
	}
	return "Invalid";
}

static void select_or_insert_card_type(sqlite3_context *ctx, int argc, sqlite3_value **argv) {
	const char *detailed;
	sqlite3 *db;
	sqlite3_stmt *stmt;
	char *sql;
	int rc;

	assert(argc == 1);

	detailed = (const char *) sqlite3_value_text(argv[0]);
	if (!detailed) {
		sqlite3_result_error(ctx, "missing detailed type", -1);
		return;
	}

	db = sqlite3_context_db_handle(ctx);

	sql = sqlite3_mprintf("SELECT id FROM DetailedType WHERE name=?");
	rc = sqlite3_prepare_v2(db, sql, -1, &stmt, NULL);
	rc |= sqlite3_bind_text(stmt, 1, detailed, -1, SQLITE_STATIC);
	if (rc != SQLITE_OK) {
		char str[256];
		sprintf(str, "preparing select failed: %d - %s", rc, sqlite3_errmsg(db));
		sqlite3_result_error(ctx, str, -1);
	} else if ((rc = sqlite3_step(stmt)) == SQLITE_ROW) {
		sqlite3_result_int64(ctx, sqlite3_column_int64(stmt, 0));
	} else if (rc == SQLITE_DONE) {
		sqlite3_free(sql);
		sqlite3_finalize(stmt);
		sql = sqlite3_mprintf(
				"INSERT INTO DetailedType(name, general_type) VALUES (?, (SELECT id FROM CardType WHERE name=?))");
		rc = sqlite3_prepare_v2(db, sql, -1, &stmt, NULL);
		rc |= sqlite3_bind_text(stmt, 1, detailed, -1, SQLITE_STATIC);
		rc |= sqlite3_bind_text(stmt, 2, generalize_type(detailed), -1, SQLITE_STATIC);
		if (rc != SQLITE_OK) {
			char str[256];
			sprintf(str, "preparing insert failed: %d - %s", rc, sqlite3_errmsg(db));
			sqlite3_result_error(ctx, str, -1);
		} else if ((rc = sqlite3_step(stmt)) == SQLITE_DONE) {
			sqlite3_result_int64(ctx, sqlite3_last_insert_rowid(db));
		} else {
			char str[256];
			sprintf(str, "inserting failed: %d - %s", rc, sqlite3_errmsg(db));
			sqlite3_result_error(ctx, str, -1);
		}
	} else {
		char str[256];
		sprintf(str, "selecting failed: %d - %s", rc, sqlite3_errmsg(db));
		sqlite3_result_error(ctx, str, -1);
	}
	sqlite3_free(sql);
	sqlite3_finalize(stmt);
	return;
}

int sqlite3_extension_init(sqlite3 *db, char **err, const sqlite3_api_routines *api) {
	int rc;
	SQLITE_EXTENSION_INIT2(api);
	/* see https://sqlite.org/c3ref/create_function.html */
	rc = sqlite3_create_function(
			db,									/* db */
			"select_or_insert",					/* zFunctionName */
			2,									/* nArg */
			SQLITE_UTF8 | SQLITE_DETERMINISTIC,	/* eTextRep */
			NULL,								/* pApp */
			select_or_insert,					/* xFunc */
			NULL,								/* xStep */
			NULL);								/* xFinal */
	if (rc != 0) {
		fprintf(stderr, "registering select_or_insert function returned %d\n", rc);
		return rc;
	}
	return sqlite3_create_function(
			db,									/* db */
			"select_or_insert_card_type",		/* zFunctionName */
			1,									/* nArg */
			SQLITE_UTF8 | SQLITE_DETERMINISTIC,	/* eTextRep */
			NULL,								/* pApp */
			select_or_insert_card_type,			/* xFunc */
			NULL,								/* xStep */
			NULL);								/* xFinal */
}
