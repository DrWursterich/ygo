# ygo

cli utility tools for the yu-gi-oh trading card game.

## Features

the features of this repository are executable shell scripts in the `bin` directory, namingly:

### random\_deck

composes a random yugioh deck from cards released in the tcg as list or as `.ydk` file.

### ygo

combines the following functions:
- __display__|__display_all__ uses `display` to show an/all image(s) of a card
- __sets__ returns all sets a card appeard in
- __query__ returns all card names that fit given filter criteria

### draftsimulator

simulates a draft given a card pool and values for amount of other players, pile size ect.
the other players draft random cards while the player gets to choose each time to see what decks could look like under the given circumstances.

## Runtime Dependencies

- `jq`
- `curl`
- `sqlite3`
- `mktemp`

It is also recommended to use an `sqlite3` version, that was compiled with the json1 extension. Otherwise database updates from `random_deck` will take several minutes instead of a couple seconds.  
You can check this by running
```bash
sqlite3 "$any_sqlite_file" "PRAGMA compile_options" | grep -coF 'ENABLE_JSON1'
```
Alternatively `random_deck` will notify you about an absence of this extension.

## Build

install the __dependencies__:
- `libsqlite3-dev`

then run `make build` to build the sqlite3 extension.

## Install

first [build](#build) the sqlite3 extension and then run `make install`.

## Uninstall

run `make uninstall`.
