
.PHONY: clean
clean:
	+$(MAKE) clean -C sqlite/extensions/select_or_insert
	+$(MAKE) clean -C bin

.PHONY: build
build:
	+$(MAKE) build -C sqlite/extensions/select_or_insert
	+$(MAKE) build -C bin

.PHONY: install
install:
	+$(MAKE) install -C sqlite/extensions/select_or_insert
	+$(MAKE) install -C bin

.PHONY: uninstall
uninstall:
	+$(MAKE) uninstall -C sqlite/extensions/select_or_insert
	+$(MAKE) uninstall -C bin

